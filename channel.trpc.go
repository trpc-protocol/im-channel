// Code generated by trpc-go/trpc-cmdline v1.0.5. DO NOT EDIT.
// source: channel.proto

package im_channel

import (
	"context"
	"errors"
	"fmt"
	reflect "reflect"

	"trpc.group/trpc-go/trpc-codec/grpc"
	_ "trpc.group/trpc-go/trpc-go"
	"trpc.group/trpc-go/trpc-go/client"
	"trpc.group/trpc-go/trpc-go/codec"
	"trpc.group/trpc-go/trpc-go/filter"
	_ "trpc.group/trpc-go/trpc-go/http"
	"trpc.group/trpc-go/trpc-go/server"
)

// START ======================================= Server Service Definition ======================================= START

// ChannelAreaService defines service.
type ChannelAreaService interface {
	// ChannelRelationMng ///////////////////////////////////////////////////频道关系域/////////////////////////////////////////////////
	// 频道关系管理
	ChannelRelationMng(ctx context.Context, req *ChannelRelationMngRequest) (*ChannelRelationMngResponse, error)
	// ExitChannel 退出频道
	ExitChannel(ctx context.Context, req *ExitChannelRequest) (*ExitChannelResponse, error)
	// GetRecommendChannel 获取推荐频道
	GetRecommendChannel(ctx context.Context, req *GetRecommendChannelRequest) (*GetRecommendChannelResponse, error)
	// GetChannelList 获取频道列表
	GetChannelList(ctx context.Context, req *GetChannelListByTypeRequest) (*GetChannelListByTypeResponse, error)
	// GetChannelVcard 获取频道名片
	GetChannelVcard(ctx context.Context, req *GetChannelVcardRequest) (*GetChannelVcardResponse, error)
	// GetUserAllChannel 获取所有频道
	GetUserAllChannel(ctx context.Context, req *GetUserAllChannelRequest) (*GetUserAllChannelResponse, error)
	// GetChannelMemberList 获取频道成员列表
	GetChannelMemberList(ctx context.Context, req *GetChannelMemberListRequest) (*GetChannelMemberListResponse, error)
	// JoinChannel 加入频道
	JoinChannel(ctx context.Context, req *BatchJoinChannelRequest) (*BatchJoinChannelResponse, error)
}

func ChannelAreaService_ChannelRelationMng_Handler(svr interface{}, ctx context.Context, f server.FilterFunc) (interface{}, error) {
	req := &ChannelRelationMngRequest{}
	filters, err := f(req)
	if err != nil {
		return nil, err
	}
	handleFunc := func(ctx context.Context, reqbody interface{}) (interface{}, error) {
		return svr.(ChannelAreaService).ChannelRelationMng(ctx, reqbody.(*ChannelRelationMngRequest))
	}

	// Try to get req from ctx.
	grpcData, grpcEnabled := ctx.Value(grpc.ContextKeyHeader).(*grpc.Header)
	if grpcEnabled {
		req = grpcData.Req.(*ChannelRelationMngRequest)
	}

	var rsp interface{}
	rsp, err = filters.Filter(ctx, req, handleFunc)
	if err != nil {
		return nil, err
	}
	// Set rsp to ctx, and set rsp empty.
	if grpcEnabled {
		grpcData.Rsp = rsp
		rsp = &ChannelRelationMngResponse{}
	}
	return rsp, nil
}

func ChannelAreaService_ExitChannel_Handler(svr interface{}, ctx context.Context, f server.FilterFunc) (interface{}, error) {
	req := &ExitChannelRequest{}
	filters, err := f(req)
	if err != nil {
		return nil, err
	}
	handleFunc := func(ctx context.Context, reqbody interface{}) (interface{}, error) {
		return svr.(ChannelAreaService).ExitChannel(ctx, reqbody.(*ExitChannelRequest))
	}

	// Try to get req from ctx.
	grpcData, grpcEnabled := ctx.Value(grpc.ContextKeyHeader).(*grpc.Header)
	if grpcEnabled {
		req = grpcData.Req.(*ExitChannelRequest)
	}

	var rsp interface{}
	rsp, err = filters.Filter(ctx, req, handleFunc)
	if err != nil {
		return nil, err
	}
	// Set rsp to ctx, and set rsp empty.
	if grpcEnabled {
		grpcData.Rsp = rsp
		rsp = &ExitChannelResponse{}
	}
	return rsp, nil
}

func ChannelAreaService_GetRecommendChannel_Handler(svr interface{}, ctx context.Context, f server.FilterFunc) (interface{}, error) {
	req := &GetRecommendChannelRequest{}
	filters, err := f(req)
	if err != nil {
		return nil, err
	}
	handleFunc := func(ctx context.Context, reqbody interface{}) (interface{}, error) {
		return svr.(ChannelAreaService).GetRecommendChannel(ctx, reqbody.(*GetRecommendChannelRequest))
	}

	// Try to get req from ctx.
	grpcData, grpcEnabled := ctx.Value(grpc.ContextKeyHeader).(*grpc.Header)
	if grpcEnabled {
		req = grpcData.Req.(*GetRecommendChannelRequest)
	}

	var rsp interface{}
	rsp, err = filters.Filter(ctx, req, handleFunc)
	if err != nil {
		return nil, err
	}
	// Set rsp to ctx, and set rsp empty.
	if grpcEnabled {
		grpcData.Rsp = rsp
		rsp = &GetRecommendChannelResponse{}
	}
	return rsp, nil
}

func ChannelAreaService_GetChannelList_Handler(svr interface{}, ctx context.Context, f server.FilterFunc) (interface{}, error) {
	req := &GetChannelListByTypeRequest{}
	filters, err := f(req)
	if err != nil {
		return nil, err
	}
	handleFunc := func(ctx context.Context, reqbody interface{}) (interface{}, error) {
		return svr.(ChannelAreaService).GetChannelList(ctx, reqbody.(*GetChannelListByTypeRequest))
	}

	// Try to get req from ctx.
	grpcData, grpcEnabled := ctx.Value(grpc.ContextKeyHeader).(*grpc.Header)
	if grpcEnabled {
		req = grpcData.Req.(*GetChannelListByTypeRequest)
	}

	var rsp interface{}
	rsp, err = filters.Filter(ctx, req, handleFunc)
	if err != nil {
		return nil, err
	}
	// Set rsp to ctx, and set rsp empty.
	if grpcEnabled {
		grpcData.Rsp = rsp
		rsp = &GetChannelListByTypeResponse{}
	}
	return rsp, nil
}

func ChannelAreaService_GetChannelVcard_Handler(svr interface{}, ctx context.Context, f server.FilterFunc) (interface{}, error) {
	req := &GetChannelVcardRequest{}
	filters, err := f(req)
	if err != nil {
		return nil, err
	}
	handleFunc := func(ctx context.Context, reqbody interface{}) (interface{}, error) {
		return svr.(ChannelAreaService).GetChannelVcard(ctx, reqbody.(*GetChannelVcardRequest))
	}

	// Try to get req from ctx.
	grpcData, grpcEnabled := ctx.Value(grpc.ContextKeyHeader).(*grpc.Header)
	if grpcEnabled {
		req = grpcData.Req.(*GetChannelVcardRequest)
	}

	var rsp interface{}
	rsp, err = filters.Filter(ctx, req, handleFunc)
	if err != nil {
		return nil, err
	}
	// Set rsp to ctx, and set rsp empty.
	if grpcEnabled {
		grpcData.Rsp = rsp
		rsp = &GetChannelVcardResponse{}
	}
	return rsp, nil
}

func ChannelAreaService_GetUserAllChannel_Handler(svr interface{}, ctx context.Context, f server.FilterFunc) (interface{}, error) {
	req := &GetUserAllChannelRequest{}
	filters, err := f(req)
	if err != nil {
		return nil, err
	}
	handleFunc := func(ctx context.Context, reqbody interface{}) (interface{}, error) {
		return svr.(ChannelAreaService).GetUserAllChannel(ctx, reqbody.(*GetUserAllChannelRequest))
	}

	// Try to get req from ctx.
	grpcData, grpcEnabled := ctx.Value(grpc.ContextKeyHeader).(*grpc.Header)
	if grpcEnabled {
		req = grpcData.Req.(*GetUserAllChannelRequest)
	}

	var rsp interface{}
	rsp, err = filters.Filter(ctx, req, handleFunc)
	if err != nil {
		return nil, err
	}
	// Set rsp to ctx, and set rsp empty.
	if grpcEnabled {
		grpcData.Rsp = rsp
		rsp = &GetUserAllChannelResponse{}
	}
	return rsp, nil
}

func ChannelAreaService_GetChannelMemberList_Handler(svr interface{}, ctx context.Context, f server.FilterFunc) (interface{}, error) {
	req := &GetChannelMemberListRequest{}
	filters, err := f(req)
	if err != nil {
		return nil, err
	}
	handleFunc := func(ctx context.Context, reqbody interface{}) (interface{}, error) {
		return svr.(ChannelAreaService).GetChannelMemberList(ctx, reqbody.(*GetChannelMemberListRequest))
	}

	// Try to get req from ctx.
	grpcData, grpcEnabled := ctx.Value(grpc.ContextKeyHeader).(*grpc.Header)
	if grpcEnabled {
		req = grpcData.Req.(*GetChannelMemberListRequest)
	}

	var rsp interface{}
	rsp, err = filters.Filter(ctx, req, handleFunc)
	if err != nil {
		return nil, err
	}
	// Set rsp to ctx, and set rsp empty.
	if grpcEnabled {
		grpcData.Rsp = rsp
		rsp = &GetChannelMemberListResponse{}
	}
	return rsp, nil
}

func ChannelAreaService_JoinChannel_Handler(svr interface{}, ctx context.Context, f server.FilterFunc) (interface{}, error) {
	req := &BatchJoinChannelRequest{}
	filters, err := f(req)
	if err != nil {
		return nil, err
	}
	handleFunc := func(ctx context.Context, reqbody interface{}) (interface{}, error) {
		return svr.(ChannelAreaService).JoinChannel(ctx, reqbody.(*BatchJoinChannelRequest))
	}

	// Try to get req from ctx.
	grpcData, grpcEnabled := ctx.Value(grpc.ContextKeyHeader).(*grpc.Header)
	if grpcEnabled {
		req = grpcData.Req.(*BatchJoinChannelRequest)
	}

	var rsp interface{}
	rsp, err = filters.Filter(ctx, req, handleFunc)
	if err != nil {
		return nil, err
	}
	// Set rsp to ctx, and set rsp empty.
	if grpcEnabled {
		grpcData.Rsp = rsp
		rsp = &BatchJoinChannelResponse{}
	}
	return rsp, nil
}

// ChannelAreaServer_ServiceDesc descriptor for server.RegisterService.
var ChannelAreaServer_ServiceDesc = server.ServiceDesc{
	ServiceName: "trpc.im.channel.ChannelArea",
	HandlerType: ((*ChannelAreaService)(nil)),
	Methods: []server.Method{
		{
			Name: "/trpc.im.channel.ChannelArea/ChannelRelationMng",
			Func: ChannelAreaService_ChannelRelationMng_Handler,
		},
		{
			Name: "/trpc.im.channel.ChannelArea/ExitChannel",
			Func: ChannelAreaService_ExitChannel_Handler,
		},
		{
			Name: "/trpc.im.channel.ChannelArea/GetRecommendChannel",
			Func: ChannelAreaService_GetRecommendChannel_Handler,
		},
		{
			Name: "/trpc.im.channel.ChannelArea/GetChannelList",
			Func: ChannelAreaService_GetChannelList_Handler,
		},
		{
			Name: "/trpc.im.channel.ChannelArea/GetChannelVcard",
			Func: ChannelAreaService_GetChannelVcard_Handler,
		},
		{
			Name: "/trpc.im.channel.ChannelArea/GetUserAllChannel",
			Func: ChannelAreaService_GetUserAllChannel_Handler,
		},
		{
			Name: "/trpc.im.channel.ChannelArea/GetChannelMemberList",
			Func: ChannelAreaService_GetChannelMemberList_Handler,
		},
		{
			Name: "/trpc.im.channel.ChannelArea/JoinChannel",
			Func: ChannelAreaService_JoinChannel_Handler,
		},
	},
}

// RegisterChannelAreaService registers service.
func RegisterChannelAreaService(s server.Service, svr ChannelAreaService) {
	if err := s.Register(&ChannelAreaServer_ServiceDesc, svr); err != nil {
		panic(fmt.Sprintf("ChannelArea register error:%v", err))
	}
	// register service to grpc
	if err := grpc.Register(ChannelAreaServer_ServiceDesc.ServiceName,
		"channel.proto",
		[]grpc.RegisterMethodsInfo{
			{
				Method: server.Method{
					Name: "ChannelRelationMng",
					Func: ChannelAreaService_ChannelRelationMng_Handler,
				},
				ReqType: reflect.TypeOf(ChannelRelationMngRequest{}),
				RspType: reflect.TypeOf(ChannelRelationMngResponse{}),
			},
			{
				Method: server.Method{
					Name: "ExitChannel",
					Func: ChannelAreaService_ExitChannel_Handler,
				},
				ReqType: reflect.TypeOf(ExitChannelRequest{}),
				RspType: reflect.TypeOf(ExitChannelResponse{}),
			},
			{
				Method: server.Method{
					Name: "GetRecommendChannel",
					Func: ChannelAreaService_GetRecommendChannel_Handler,
				},
				ReqType: reflect.TypeOf(GetRecommendChannelRequest{}),
				RspType: reflect.TypeOf(GetRecommendChannelResponse{}),
			},
			{
				Method: server.Method{
					Name: "GetChannelList",
					Func: ChannelAreaService_GetChannelList_Handler,
				},
				ReqType: reflect.TypeOf(GetChannelListByTypeRequest{}),
				RspType: reflect.TypeOf(GetChannelListByTypeResponse{}),
			},
			{
				Method: server.Method{
					Name: "GetChannelVcard",
					Func: ChannelAreaService_GetChannelVcard_Handler,
				},
				ReqType: reflect.TypeOf(GetChannelVcardRequest{}),
				RspType: reflect.TypeOf(GetChannelVcardResponse{}),
			},
			{
				Method: server.Method{
					Name: "GetUserAllChannel",
					Func: ChannelAreaService_GetUserAllChannel_Handler,
				},
				ReqType: reflect.TypeOf(GetUserAllChannelRequest{}),
				RspType: reflect.TypeOf(GetUserAllChannelResponse{}),
			},
			{
				Method: server.Method{
					Name: "GetChannelMemberList",
					Func: ChannelAreaService_GetChannelMemberList_Handler,
				},
				ReqType: reflect.TypeOf(GetChannelMemberListRequest{}),
				RspType: reflect.TypeOf(GetChannelMemberListResponse{}),
			},
			{
				Method: server.Method{
					Name: "JoinChannel",
					Func: ChannelAreaService_JoinChannel_Handler,
				},
				ReqType: reflect.TypeOf(BatchJoinChannelRequest{}),
				RspType: reflect.TypeOf(BatchJoinChannelResponse{}),
			},
		}); err != nil {
		panic(fmt.Sprintf("grpc register ChannelArea error:%v", err))
	}
	if err := grpc.RegisterStream(ChannelAreaServer_ServiceDesc.ServiceName,
		"channel.proto",
		ChannelAreaServer_ServiceDesc.Streams,
		svr, (*ChannelAreaService)(nil)); err != nil {
		panic(fmt.Sprintf("grpc register ChannelArea streams error:%v", err))
	}
}

// START --------------------------------- Default Unimplemented Server Service --------------------------------- START

type UnimplementedChannelArea struct{}

// ChannelRelationMng ///////////////////////////////////////////////////频道关系域/////////////////////////////////////////////////
// 频道关系管理
func (s *UnimplementedChannelArea) ChannelRelationMng(ctx context.Context, req *ChannelRelationMngRequest) (*ChannelRelationMngResponse, error) {
	return nil, errors.New("rpc ChannelRelationMng of service ChannelArea is not implemented")
}

// ExitChannel 退出频道
func (s *UnimplementedChannelArea) ExitChannel(ctx context.Context, req *ExitChannelRequest) (*ExitChannelResponse, error) {
	return nil, errors.New("rpc ExitChannel of service ChannelArea is not implemented")
}

// GetRecommendChannel 获取推荐频道
func (s *UnimplementedChannelArea) GetRecommendChannel(ctx context.Context, req *GetRecommendChannelRequest) (*GetRecommendChannelResponse, error) {
	return nil, errors.New("rpc GetRecommendChannel of service ChannelArea is not implemented")
}

// GetChannelList 获取频道列表
func (s *UnimplementedChannelArea) GetChannelList(ctx context.Context, req *GetChannelListByTypeRequest) (*GetChannelListByTypeResponse, error) {
	return nil, errors.New("rpc GetChannelList of service ChannelArea is not implemented")
}

// GetChannelVcard 获取频道名片
func (s *UnimplementedChannelArea) GetChannelVcard(ctx context.Context, req *GetChannelVcardRequest) (*GetChannelVcardResponse, error) {
	return nil, errors.New("rpc GetChannelVcard of service ChannelArea is not implemented")
}

// GetUserAllChannel 获取所有频道
func (s *UnimplementedChannelArea) GetUserAllChannel(ctx context.Context, req *GetUserAllChannelRequest) (*GetUserAllChannelResponse, error) {
	return nil, errors.New("rpc GetUserAllChannel of service ChannelArea is not implemented")
}

// GetChannelMemberList 获取频道成员列表
func (s *UnimplementedChannelArea) GetChannelMemberList(ctx context.Context, req *GetChannelMemberListRequest) (*GetChannelMemberListResponse, error) {
	return nil, errors.New("rpc GetChannelMemberList of service ChannelArea is not implemented")
}

// JoinChannel 加入频道
func (s *UnimplementedChannelArea) JoinChannel(ctx context.Context, req *BatchJoinChannelRequest) (*BatchJoinChannelResponse, error) {
	return nil, errors.New("rpc JoinChannel of service ChannelArea is not implemented")
}

// END --------------------------------- Default Unimplemented Server Service --------------------------------- END

// END ======================================= Server Service Definition ======================================= END

// START ======================================= Client Service Definition ======================================= START

// ChannelAreaClientProxy defines service client proxy
type ChannelAreaClientProxy interface {
	// ChannelRelationMng ///////////////////////////////////////////////////频道关系域/////////////////////////////////////////////////
	// 频道关系管理
	ChannelRelationMng(ctx context.Context, req *ChannelRelationMngRequest, opts ...client.Option) (rsp *ChannelRelationMngResponse, err error)
	// ExitChannel 退出频道
	ExitChannel(ctx context.Context, req *ExitChannelRequest, opts ...client.Option) (rsp *ExitChannelResponse, err error)
	// GetRecommendChannel 获取推荐频道
	GetRecommendChannel(ctx context.Context, req *GetRecommendChannelRequest, opts ...client.Option) (rsp *GetRecommendChannelResponse, err error)
	// GetChannelList 获取频道列表
	GetChannelList(ctx context.Context, req *GetChannelListByTypeRequest, opts ...client.Option) (rsp *GetChannelListByTypeResponse, err error)
	// GetChannelVcard 获取频道名片
	GetChannelVcard(ctx context.Context, req *GetChannelVcardRequest, opts ...client.Option) (rsp *GetChannelVcardResponse, err error)
	// GetUserAllChannel 获取所有频道
	GetUserAllChannel(ctx context.Context, req *GetUserAllChannelRequest, opts ...client.Option) (rsp *GetUserAllChannelResponse, err error)
	// GetChannelMemberList 获取频道成员列表
	GetChannelMemberList(ctx context.Context, req *GetChannelMemberListRequest, opts ...client.Option) (rsp *GetChannelMemberListResponse, err error)
	// JoinChannel 加入频道
	JoinChannel(ctx context.Context, req *BatchJoinChannelRequest, opts ...client.Option) (rsp *BatchJoinChannelResponse, err error)
}

type ChannelAreaClientProxyImpl struct {
	client client.Client
	opts   []client.Option
}

var NewChannelAreaClientProxy = func(opts ...client.Option) ChannelAreaClientProxy {
	return &ChannelAreaClientProxyImpl{client: client.DefaultClient, opts: opts}
}

func (c *ChannelAreaClientProxyImpl) ChannelRelationMng(ctx context.Context, req *ChannelRelationMngRequest, opts ...client.Option) (*ChannelRelationMngResponse, error) {
	// set req to ctx
	h := ctx.Value(grpc.ContextKeyHeader)
	var header *grpc.Header
	if h == nil {
		header = &grpc.Header{}
		ctx = context.WithValue(ctx, grpc.ContextKeyHeader, header)
	} else {
		var ok bool
		header, ok = h.(*grpc.Header)
		if !ok {
			return nil, errors.New(fmt.Sprintf("grpc header in context cannot be transferred to grpc.Header"))
		}
	}
	header.Req = req
	header.Rsp = &ChannelRelationMngResponse{}
	ctx, msg := codec.WithCloneMessage(ctx)
	defer codec.PutBackMessage(msg)
	msg.WithClientRPCName("/trpc.im.channel.ChannelArea/ChannelRelationMng")
	msg.WithCalleeServiceName(ChannelAreaServer_ServiceDesc.ServiceName)
	msg.WithCalleeApp("im")
	msg.WithCalleeServer("channel")
	msg.WithCalleeService("ChannelArea")
	msg.WithCalleeMethod("ChannelRelationMng")
	msg.WithSerializationType(codec.SerializationTypePB)
	callopts := make([]client.Option, 0, len(c.opts)+len(opts))
	callopts = append(callopts, c.opts...)
	callopts = append(callopts, opts...)
	rsp := &ChannelRelationMngResponse{}
	callopts = append(callopts, client.WithProtocol("grpc"))
	callopts = append(callopts, client.WithFilter(func(ctx context.Context, req1, rsp1 interface{}, next filter.ClientHandleFunc) error {
		err := next(ctx, req1, rsp1)
		header := ctx.Value(grpc.ContextKeyHeader).(*grpc.Header)
		*rsp = *header.Rsp.(*ChannelRelationMngResponse)
		return err
	}))
	if err := c.client.Invoke(ctx, req, rsp, callopts...); err != nil {
		return nil, err
	}
	return rsp, nil
}

func (c *ChannelAreaClientProxyImpl) ExitChannel(ctx context.Context, req *ExitChannelRequest, opts ...client.Option) (*ExitChannelResponse, error) {
	// set req to ctx
	h := ctx.Value(grpc.ContextKeyHeader)
	var header *grpc.Header
	if h == nil {
		header = &grpc.Header{}
		ctx = context.WithValue(ctx, grpc.ContextKeyHeader, header)
	} else {
		var ok bool
		header, ok = h.(*grpc.Header)
		if !ok {
			return nil, errors.New(fmt.Sprintf("grpc header in context cannot be transferred to grpc.Header"))
		}
	}
	header.Req = req
	header.Rsp = &ExitChannelResponse{}
	ctx, msg := codec.WithCloneMessage(ctx)
	defer codec.PutBackMessage(msg)
	msg.WithClientRPCName("/trpc.im.channel.ChannelArea/ExitChannel")
	msg.WithCalleeServiceName(ChannelAreaServer_ServiceDesc.ServiceName)
	msg.WithCalleeApp("im")
	msg.WithCalleeServer("channel")
	msg.WithCalleeService("ChannelArea")
	msg.WithCalleeMethod("ExitChannel")
	msg.WithSerializationType(codec.SerializationTypePB)
	callopts := make([]client.Option, 0, len(c.opts)+len(opts))
	callopts = append(callopts, c.opts...)
	callopts = append(callopts, opts...)
	rsp := &ExitChannelResponse{}
	callopts = append(callopts, client.WithProtocol("grpc"))
	callopts = append(callopts, client.WithFilter(func(ctx context.Context, req1, rsp1 interface{}, next filter.ClientHandleFunc) error {
		err := next(ctx, req1, rsp1)
		header := ctx.Value(grpc.ContextKeyHeader).(*grpc.Header)
		*rsp = *header.Rsp.(*ExitChannelResponse)
		return err
	}))
	if err := c.client.Invoke(ctx, req, rsp, callopts...); err != nil {
		return nil, err
	}
	return rsp, nil
}

func (c *ChannelAreaClientProxyImpl) GetRecommendChannel(ctx context.Context, req *GetRecommendChannelRequest, opts ...client.Option) (*GetRecommendChannelResponse, error) {
	// set req to ctx
	h := ctx.Value(grpc.ContextKeyHeader)
	var header *grpc.Header
	if h == nil {
		header = &grpc.Header{}
		ctx = context.WithValue(ctx, grpc.ContextKeyHeader, header)
	} else {
		var ok bool
		header, ok = h.(*grpc.Header)
		if !ok {
			return nil, errors.New(fmt.Sprintf("grpc header in context cannot be transferred to grpc.Header"))
		}
	}
	header.Req = req
	header.Rsp = &GetRecommendChannelResponse{}
	ctx, msg := codec.WithCloneMessage(ctx)
	defer codec.PutBackMessage(msg)
	msg.WithClientRPCName("/trpc.im.channel.ChannelArea/GetRecommendChannel")
	msg.WithCalleeServiceName(ChannelAreaServer_ServiceDesc.ServiceName)
	msg.WithCalleeApp("im")
	msg.WithCalleeServer("channel")
	msg.WithCalleeService("ChannelArea")
	msg.WithCalleeMethod("GetRecommendChannel")
	msg.WithSerializationType(codec.SerializationTypePB)
	callopts := make([]client.Option, 0, len(c.opts)+len(opts))
	callopts = append(callopts, c.opts...)
	callopts = append(callopts, opts...)
	rsp := &GetRecommendChannelResponse{}
	callopts = append(callopts, client.WithProtocol("grpc"))
	callopts = append(callopts, client.WithFilter(func(ctx context.Context, req1, rsp1 interface{}, next filter.ClientHandleFunc) error {
		err := next(ctx, req1, rsp1)
		header := ctx.Value(grpc.ContextKeyHeader).(*grpc.Header)
		*rsp = *header.Rsp.(*GetRecommendChannelResponse)
		return err
	}))
	if err := c.client.Invoke(ctx, req, rsp, callopts...); err != nil {
		return nil, err
	}
	return rsp, nil
}

func (c *ChannelAreaClientProxyImpl) GetChannelList(ctx context.Context, req *GetChannelListByTypeRequest, opts ...client.Option) (*GetChannelListByTypeResponse, error) {
	// set req to ctx
	h := ctx.Value(grpc.ContextKeyHeader)
	var header *grpc.Header
	if h == nil {
		header = &grpc.Header{}
		ctx = context.WithValue(ctx, grpc.ContextKeyHeader, header)
	} else {
		var ok bool
		header, ok = h.(*grpc.Header)
		if !ok {
			return nil, errors.New(fmt.Sprintf("grpc header in context cannot be transferred to grpc.Header"))
		}
	}
	header.Req = req
	header.Rsp = &GetChannelListByTypeResponse{}
	ctx, msg := codec.WithCloneMessage(ctx)
	defer codec.PutBackMessage(msg)
	msg.WithClientRPCName("/trpc.im.channel.ChannelArea/GetChannelList")
	msg.WithCalleeServiceName(ChannelAreaServer_ServiceDesc.ServiceName)
	msg.WithCalleeApp("im")
	msg.WithCalleeServer("channel")
	msg.WithCalleeService("ChannelArea")
	msg.WithCalleeMethod("GetChannelList")
	msg.WithSerializationType(codec.SerializationTypePB)
	callopts := make([]client.Option, 0, len(c.opts)+len(opts))
	callopts = append(callopts, c.opts...)
	callopts = append(callopts, opts...)
	rsp := &GetChannelListByTypeResponse{}
	callopts = append(callopts, client.WithProtocol("grpc"))
	callopts = append(callopts, client.WithFilter(func(ctx context.Context, req1, rsp1 interface{}, next filter.ClientHandleFunc) error {
		err := next(ctx, req1, rsp1)
		header := ctx.Value(grpc.ContextKeyHeader).(*grpc.Header)
		*rsp = *header.Rsp.(*GetChannelListByTypeResponse)
		return err
	}))
	if err := c.client.Invoke(ctx, req, rsp, callopts...); err != nil {
		return nil, err
	}
	return rsp, nil
}

func (c *ChannelAreaClientProxyImpl) GetChannelVcard(ctx context.Context, req *GetChannelVcardRequest, opts ...client.Option) (*GetChannelVcardResponse, error) {
	// set req to ctx
	h := ctx.Value(grpc.ContextKeyHeader)
	var header *grpc.Header
	if h == nil {
		header = &grpc.Header{}
		ctx = context.WithValue(ctx, grpc.ContextKeyHeader, header)
	} else {
		var ok bool
		header, ok = h.(*grpc.Header)
		if !ok {
			return nil, errors.New(fmt.Sprintf("grpc header in context cannot be transferred to grpc.Header"))
		}
	}
	header.Req = req
	header.Rsp = &GetChannelVcardResponse{}
	ctx, msg := codec.WithCloneMessage(ctx)
	defer codec.PutBackMessage(msg)
	msg.WithClientRPCName("/trpc.im.channel.ChannelArea/GetChannelVcard")
	msg.WithCalleeServiceName(ChannelAreaServer_ServiceDesc.ServiceName)
	msg.WithCalleeApp("im")
	msg.WithCalleeServer("channel")
	msg.WithCalleeService("ChannelArea")
	msg.WithCalleeMethod("GetChannelVcard")
	msg.WithSerializationType(codec.SerializationTypePB)
	callopts := make([]client.Option, 0, len(c.opts)+len(opts))
	callopts = append(callopts, c.opts...)
	callopts = append(callopts, opts...)
	rsp := &GetChannelVcardResponse{}
	callopts = append(callopts, client.WithProtocol("grpc"))
	callopts = append(callopts, client.WithFilter(func(ctx context.Context, req1, rsp1 interface{}, next filter.ClientHandleFunc) error {
		err := next(ctx, req1, rsp1)
		header := ctx.Value(grpc.ContextKeyHeader).(*grpc.Header)
		*rsp = *header.Rsp.(*GetChannelVcardResponse)
		return err
	}))
	if err := c.client.Invoke(ctx, req, rsp, callopts...); err != nil {
		return nil, err
	}
	return rsp, nil
}

func (c *ChannelAreaClientProxyImpl) GetUserAllChannel(ctx context.Context, req *GetUserAllChannelRequest, opts ...client.Option) (*GetUserAllChannelResponse, error) {
	// set req to ctx
	h := ctx.Value(grpc.ContextKeyHeader)
	var header *grpc.Header
	if h == nil {
		header = &grpc.Header{}
		ctx = context.WithValue(ctx, grpc.ContextKeyHeader, header)
	} else {
		var ok bool
		header, ok = h.(*grpc.Header)
		if !ok {
			return nil, errors.New(fmt.Sprintf("grpc header in context cannot be transferred to grpc.Header"))
		}
	}
	header.Req = req
	header.Rsp = &GetUserAllChannelResponse{}
	ctx, msg := codec.WithCloneMessage(ctx)
	defer codec.PutBackMessage(msg)
	msg.WithClientRPCName("/trpc.im.channel.ChannelArea/GetUserAllChannel")
	msg.WithCalleeServiceName(ChannelAreaServer_ServiceDesc.ServiceName)
	msg.WithCalleeApp("im")
	msg.WithCalleeServer("channel")
	msg.WithCalleeService("ChannelArea")
	msg.WithCalleeMethod("GetUserAllChannel")
	msg.WithSerializationType(codec.SerializationTypePB)
	callopts := make([]client.Option, 0, len(c.opts)+len(opts))
	callopts = append(callopts, c.opts...)
	callopts = append(callopts, opts...)
	rsp := &GetUserAllChannelResponse{}
	callopts = append(callopts, client.WithProtocol("grpc"))
	callopts = append(callopts, client.WithFilter(func(ctx context.Context, req1, rsp1 interface{}, next filter.ClientHandleFunc) error {
		err := next(ctx, req1, rsp1)
		header := ctx.Value(grpc.ContextKeyHeader).(*grpc.Header)
		*rsp = *header.Rsp.(*GetUserAllChannelResponse)
		return err
	}))
	if err := c.client.Invoke(ctx, req, rsp, callopts...); err != nil {
		return nil, err
	}
	return rsp, nil
}

func (c *ChannelAreaClientProxyImpl) GetChannelMemberList(ctx context.Context, req *GetChannelMemberListRequest, opts ...client.Option) (*GetChannelMemberListResponse, error) {
	// set req to ctx
	h := ctx.Value(grpc.ContextKeyHeader)
	var header *grpc.Header
	if h == nil {
		header = &grpc.Header{}
		ctx = context.WithValue(ctx, grpc.ContextKeyHeader, header)
	} else {
		var ok bool
		header, ok = h.(*grpc.Header)
		if !ok {
			return nil, errors.New(fmt.Sprintf("grpc header in context cannot be transferred to grpc.Header"))
		}
	}
	header.Req = req
	header.Rsp = &GetChannelMemberListResponse{}
	ctx, msg := codec.WithCloneMessage(ctx)
	defer codec.PutBackMessage(msg)
	msg.WithClientRPCName("/trpc.im.channel.ChannelArea/GetChannelMemberList")
	msg.WithCalleeServiceName(ChannelAreaServer_ServiceDesc.ServiceName)
	msg.WithCalleeApp("im")
	msg.WithCalleeServer("channel")
	msg.WithCalleeService("ChannelArea")
	msg.WithCalleeMethod("GetChannelMemberList")
	msg.WithSerializationType(codec.SerializationTypePB)
	callopts := make([]client.Option, 0, len(c.opts)+len(opts))
	callopts = append(callopts, c.opts...)
	callopts = append(callopts, opts...)
	rsp := &GetChannelMemberListResponse{}
	callopts = append(callopts, client.WithProtocol("grpc"))
	callopts = append(callopts, client.WithFilter(func(ctx context.Context, req1, rsp1 interface{}, next filter.ClientHandleFunc) error {
		err := next(ctx, req1, rsp1)
		header := ctx.Value(grpc.ContextKeyHeader).(*grpc.Header)
		*rsp = *header.Rsp.(*GetChannelMemberListResponse)
		return err
	}))
	if err := c.client.Invoke(ctx, req, rsp, callopts...); err != nil {
		return nil, err
	}
	return rsp, nil
}

func (c *ChannelAreaClientProxyImpl) JoinChannel(ctx context.Context, req *BatchJoinChannelRequest, opts ...client.Option) (*BatchJoinChannelResponse, error) {
	// set req to ctx
	h := ctx.Value(grpc.ContextKeyHeader)
	var header *grpc.Header
	if h == nil {
		header = &grpc.Header{}
		ctx = context.WithValue(ctx, grpc.ContextKeyHeader, header)
	} else {
		var ok bool
		header, ok = h.(*grpc.Header)
		if !ok {
			return nil, errors.New(fmt.Sprintf("grpc header in context cannot be transferred to grpc.Header"))
		}
	}
	header.Req = req
	header.Rsp = &BatchJoinChannelResponse{}
	ctx, msg := codec.WithCloneMessage(ctx)
	defer codec.PutBackMessage(msg)
	msg.WithClientRPCName("/trpc.im.channel.ChannelArea/JoinChannel")
	msg.WithCalleeServiceName(ChannelAreaServer_ServiceDesc.ServiceName)
	msg.WithCalleeApp("im")
	msg.WithCalleeServer("channel")
	msg.WithCalleeService("ChannelArea")
	msg.WithCalleeMethod("JoinChannel")
	msg.WithSerializationType(codec.SerializationTypePB)
	callopts := make([]client.Option, 0, len(c.opts)+len(opts))
	callopts = append(callopts, c.opts...)
	callopts = append(callopts, opts...)
	rsp := &BatchJoinChannelResponse{}
	callopts = append(callopts, client.WithProtocol("grpc"))
	callopts = append(callopts, client.WithFilter(func(ctx context.Context, req1, rsp1 interface{}, next filter.ClientHandleFunc) error {
		err := next(ctx, req1, rsp1)
		header := ctx.Value(grpc.ContextKeyHeader).(*grpc.Header)
		*rsp = *header.Rsp.(*BatchJoinChannelResponse)
		return err
	}))
	if err := c.client.Invoke(ctx, req, rsp, callopts...); err != nil {
		return nil, err
	}
	return rsp, nil
}

// END ======================================= Client Service Definition ======================================= END
